package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"github.com/jroimartin/gocui"
)

func main() {
	ipPtr := flag.String("ip", "", "Server IP to connect to.")
	portPtr := flag.Int("port", 0, "Server port to connect to.")
	userPtr := flag.String("user", "", "Chat username")

	flag.Parse()

	if *ipPtr == "" || *portPtr == 0 || *userPtr == "" {
		flag.PrintDefaults()
		os.Exit(0)
	}

	s, err := net.Dial("tcp", fmt.Sprintf("%s:%d", *ipPtr, *portPtr))
	if err != nil {
		log.Fatalln("Could not connect to server")
	}

	//Send username
	fmt.Fprintf(s, "USERNAME:%s\n", *userPtr)

	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.SetManagerFunc(layout)

	//read banner
	b, err := bufio.NewReader(s).ReadString(byte('B'))
	updateChatPane(b[:len(b)-1], g)

	go handleIncoming(s, g)

	// Send message
	if err := g.SetKeybinding("stdin", gocui.KeyEnter, gocui.ModNone, func(g *gocui.Gui, v *gocui.View) error {
		msg := v.Buffer()

		if strings.Contains(msg, "/quit") {
			os.Exit(1)
		}
		fmt.Fprint(s, msg)

		v.Clear()
		v.SetCursor(0, 0)
		return nil
	}); err != nil {
		log.Panicln(err)
	}

	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func updateChatPane(text string, g *gocui.Gui) {
	g.Update(func(g *gocui.Gui) error {
		v, err := g.View("Chat")
		if err != nil {
			return err
		}
		fmt.Fprintf(v, text)
		return nil
	})
}

func handleIncoming(c net.Conn, g *gocui.Gui) {
	buf := bufio.NewReader(c)

	for {
		netData, err := buf.ReadString('\n')
		if err != nil {
			log.Println("Server Down")
			os.Exit(1)
		}

		updateChatPane(netData, g)
	}
}

func layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()
	if v, err := g.SetView("Chat", 0, 1, maxX-1, maxY-5); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Wrap = true
		v.Autoscroll = true
	}
	if v, err := g.SetView("stdin", 0, maxY-4, maxX-1, maxY-2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		if _, err := g.SetCurrentView("stdin"); err != nil {
			return err
		}
		v.Editable = true
	}
	g.Cursor = true
	return nil
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}
