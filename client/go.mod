module chatclient

go 1.14

require (
	github.com/jroimartin/gocui v0.4.0
	github.com/nsf/termbox-go v1.1.0 // indirect
)
