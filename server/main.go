package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/keeZey/keezey-chat/server/internal/pkg/client"
)

func main() {
	ipPtr := flag.String("ip", "", "Server IP to listen on.")
	portPtr := flag.Int("port", 0, "Server port to listen on.")

	flag.Parse()

	if *ipPtr == "" || *portPtr == 0 {
		flag.PrintDefaults()
		os.Exit(0)
	}

	s, err := net.Listen("tcp", fmt.Sprintf("%s:%d", *ipPtr, *portPtr))

	if err != nil {
		log.Fatalln("Could not start server", err)
	}
	defer s.Close()
	fmt.Printf("Listening on %s:%d\n", *ipPtr, *portPtr)

	c := client.NewService()
	for {
		conn, err := s.Accept()
		if err != nil {
			fmt.Println("Couldnt accept: ", err)
		}
		c.NewClient(conn)
	}
}
