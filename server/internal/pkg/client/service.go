package client

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/google/uuid"
)

type iservice interface {
	NewClient(net.Conn)
	sendMessage()
	removeClient()
}

type service struct {
	clients        []client
	messagesChan   chan message
	disconnectChan chan uuid.UUID
}

type message struct {
	sender  client
	message string
}

func NewService() iservice {

	s := &service{
		messagesChan:   make(chan message),
		disconnectChan: make(chan uuid.UUID),
	}
	go s.sendMessage()
	go s.removeClient()
	return s
}

func (s *service) NewClient(c net.Conn) {
	uuid := uuid.New()

	usernameBuf, _ := bufio.NewReader(c).ReadString('\n')
	username := strings.Split(usernameBuf, ":")
	cl := client{
		uuid:     uuid,
		conn:     c,
		username: strings.TrimSpace(username[1]),
	}

	s.clients = append(s.clients, cl)
	go cl.handleConn(s.messagesChan, s.disconnectChan)

	fmt.Fprintf(cl.conn, asciiBanner)
	time.Sleep(500 * time.Millisecond)

}

func (s *service) removeClient() {
	for {
		client := <-s.disconnectChan
		for i, c := range s.clients {
			if c.uuid == client {
				fmt.Println("Client removed: ", client, c.username)
				(s.clients)[i] = (s.clients)[len(s.clients)-1]
				s.clients = (s.clients)[:len(s.clients)-1]
			}
		}
	}
}

func (s *service) sendMessage() {
	for {
		msg := <-s.messagesChan
		fmt.Println("MESSAGE", msg.message, " client length: ", len(s.clients))
		for _, c := range s.clients {
			fmt.Fprintf(c.conn, "%s > %s\n", msg.sender.username, msg.message)
		}
	}
}
