package client

import (
	"bufio"
	"fmt"
	"net"
	"strings"

	"github.com/google/uuid"
)

type client struct {
	uuid     uuid.UUID
	username string
	conn     net.Conn
}

func (c *client) handleConn(m chan<- message, d chan<- uuid.UUID) {
	buf := bufio.NewReader(c.conn)
	for {
		netData, err := buf.ReadString('\n')
		if err != nil {
			break
		}

		msg := message{
			sender:  *c,
			message: strings.TrimSpace(string(netData)),
		}

		fmt.Println("herre")

		m <- msg
	}
	d <- c.uuid
	c.conn.Close()
}
